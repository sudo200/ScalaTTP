import at.sudo200.scalattp.core.Server
import at.sudo200.scalattp.core.Server.portToInetSocketAddr

import scala.concurrent.ExecutionContext.Implicits.global

case object Main {
  def main(args: Array[String]): Unit = {

    val server = Server(8080)

    /*val router = RoutingHandler()

    router.addRoute("/static*", LocalFileHandler(new File("C:/Users/Fabian").toPath, listDirectories = true))

    server.setHandler(router)*/
    //server.setHandler(LocalFileHandler(new File("C:/Users/Fabian").toPath, listDirectories = true))

    server.start()
    server.join()
  }
}
