package at.sudo200.scalattp.protocol.http

import java.util.stream.Collectors

case class Header(private val default_entries: (String, String)*)
  extends java.util.Hashtable[String, String] with (String => Option[String]) {
  default_entries.foreach(entry => super.put(entry._1, entry._2))

  override def apply(key: String): Option[String] = Option(super.get(key))

  override def toString: String =
    super.entrySet.stream
      .map(entry => f"${entry.getKey}: ${entry.getValue}\r\n")
      .collect(Collectors.joining())
}

object Header extends ((String) => Header) {
  override def apply(header: String): Header = {
    val h = Header()
    header.trim.split("\\r*\\n+").foreach(line => {
      val Array(key, value) = line.split(":", 2).map(_.trim)
      h.put(key, value)
    })
    h
  }
}
