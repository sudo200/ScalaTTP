package at.sudo200.scalattp.handlers

import at.sudo200.scalattp.core.RequestHandler
import at.sudo200.scalattp.protocol.http.{Header, Request, Response, Status, StatusLine}
import at.sudo200.scalattp.utils.HandlerFactory

import java.net.URI
import java.util.concurrent.atomic.AtomicReference
import scala.collection.concurrent.{Map, TrieMap}

case class RoutingHandler(defaultRoutes: (String, RequestHandler)*) extends RequestHandler {
  private val routes: Map[String, RequestHandler] = TrieMap(defaultRoutes: _*)
  private val defaultRoute = new AtomicReference[RequestHandler](HandlerFactory.createStatusResponder(Status.NOT_FOUND))

  def addRoute(path: String, handler: RequestHandler): Unit = routes.put(path, handler)
  def setDefaultRoute(default: RequestHandler): Unit = defaultRoute.set(default)

  override def handle(req: Request, res: Response): Unit = {
    val path = req.statusLine.uri.getPath
    val matchingRoutes = routes.filter(key => path.matches(key._1.replaceAll("\\*", ".*")))
    if (matchingRoutes.nonEmpty) {
      val route = matchingRoutes.maxBy(_._1.length)._1
      val newReq = Request(
        StatusLine.Request(
          req.statusLine.method,
          new URI(req.statusLine.uri.toString.stripPrefix(route.replaceAll("\\*", ""))),
          req.statusLine.version
        ),
        req.header,
        req.body,
        req.client
      )
      routes(route)(newReq, res)
    } else
      defaultRoute.get()(req, res)
  }
}
