package at.sudo200.scalattp.core

import java.net.{InetSocketAddress, SocketAddress}
import java.util.concurrent.atomic.AtomicReference
import javax.net.ServerSocketFactory
import javax.net.ssl.{SSLContext, SSLServerSocket}
import scala.concurrent.ExecutionContext
import scala.language.implicitConversions

case class Server(private val addr: SocketAddress, ssl: Boolean = false)
                 (implicit val executionContext: ExecutionContext) {
  //private val LOG = Logger(getClass)

  private val requestHandler = new AtomicReference[RequestHandler](RequestHandler())
  private val socket =
    if(ssl) {
      val ctx = SSLContext.getInstance("TLS")
      ctx.init(null, null, null)
      val sock = ctx.getServerSocketFactory.createServerSocket().asInstanceOf[SSLServerSocket]
      sock.setSSLParameters(ctx.getSupportedSSLParameters)
      sock
    }
    else ServerSocketFactory.getDefault.createServerSocket()
  socket.setReuseAddress(true)
  socket.bind(addr)


  private val acceptor = Acceptor(socket, socket =>
    executionContext.execute(ClientHandler(socket, requestHandler.get))
  )
  private val acceptorThread = new Thread(acceptor, f"Acceptor[${socket}]")
  acceptorThread.setDaemon(true)

  def start(): Unit = acceptorThread.start()

  def stop(): Unit = acceptorThread.interrupt()

  def join(): Unit = acceptorThread.join()

  def setHandler(handler: RequestHandler): Unit = requestHandler.set(handler)
}

object Server {
  implicit def portToInetSocketAddr(port: Int): InetSocketAddress = new InetSocketAddress(port)
}
