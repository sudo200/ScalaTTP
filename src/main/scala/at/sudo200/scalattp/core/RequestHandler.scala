package at.sudo200.scalattp.core

import at.sudo200.scalattp.protocol.http.{Header, Request, Response}
import at.sudo200.scalattp.utils.HandlerFactory

trait RequestHandler extends ((Request, Response) => Unit) {
  override final def apply(req: Request, res: Response): Unit = handle(req, res)

  def handle(req: Request, res: Response): Unit
}

object RequestHandler extends (() => RequestHandler) {
  override def apply(): RequestHandler = (req, res) => HandlerFactory.createStringResponder(
    "<!DOCTYPE html>" +
      "<html lang=\"en\">" +
      "<head>" +
      "<title>It works!</title>" +
      "</head>" +
      "<body>" +
      "<h1>Your IP:</h1>" +
      f"<p>${req.client.getRemoteSocketAddress}</p>" +
      "</body>" +
      "</html>",
    Header("Content-Type" -> "text/html")
  )(req, res)
}
