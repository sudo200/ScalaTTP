package at.sudo200.scalattp.core

import java.net.Socket
import java.nio.channels.SocketChannel

trait ConnectionHandler extends (Socket => Unit)
