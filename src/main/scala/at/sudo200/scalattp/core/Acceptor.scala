package at.sudo200.scalattp.core

import java.net.{ServerSocket, Socket}

case class Acceptor(private val socket: ServerSocket, handlers: (Socket => Unit)*) extends Runnable {
  override def run(): Unit = while (true) {
    val sock = socket.accept()
    handlers.foreach(_.apply(sock))
  }
}
