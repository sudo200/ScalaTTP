package at.sudo200.scalattp.utils

import java.util.stream.Collectors

case class Query(private val entries: (String, String)*)
  extends java.util.Hashtable[String, String] with (String => Option[String]) {
  entries.foreach(entry => super.put(entry._1, entry._2))

  override def apply(key: String): Option[String] = Option(super.get(key))

  override def toString(): String =
    super.entrySet.stream
      .map(entry => f"${entry.getKey}=${entry.getValue}")
      .collect(Collectors.joining("&"))
}

object Query extends (String => Query) {
  override def apply(query: String): Query = {
    val q = Query()
    query.trim.split('&').foreach(entry => {
      val Array(key, value) = entry.split("=", 2)
        .map(_.trim)
      q.put(key, value)
    })
    q
  }
}
